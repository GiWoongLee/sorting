

package sorting;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class SortingTest
{
	public static void main(String args[])
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try
		{
			boolean isRandom = false;	// 입력받은 배열이 난수인가 아닌가?
			int[] value;	// 입력 받을 숫자들의 배열
			String nums = br.readLine();	// 첫 줄을 입력 받음
			if (nums.charAt(0) == 'r')
			{
				// 난수일 경우
				isRandom = true;	// 난수임을 표시

				String[] nums_arg = nums.split(" ");

				int numsize = Integer.parseInt(nums_arg[1]);	// 총 갯수
				int rminimum = Integer.parseInt(nums_arg[2]);	// 최소값
				int rmaximum = Integer.parseInt(nums_arg[3]);	// 최대값

				Random rand = new Random();	// 난수 인스턴스를 생성한다.

				value = new int[numsize];	// 배열을 생성한다.
				for (int i = 0; i < value.length; i++)	// 각각의 배열에 난수를 생성하여 대입
					value[i] = rand.nextInt(rmaximum - rminimum + 1) + rminimum;
			}
			else
			{
				// 난수가 아닐 경우
				int numsize = Integer.parseInt(nums);

				value = new int[numsize];	// 배열을 생성한다.
				for (int i = 0; i < value.length; i++)	// 한줄씩 입력받아 배열원소로 대입
					value[i] = Integer.parseInt(br.readLine());
			}

			// 숫자 입력을 다 받았으므로 정렬 방법을 받아 그에 맞는 정렬을 수행한다.
			while (true)
			{
				int[] newvalue = (int[])value.clone();	// 원래 값의 보호를 위해 복사본을 생성한다.

				String command = br.readLine();

				long t = System.currentTimeMillis();
				switch (command.charAt(0))
				{
					case 'B':	// Bubble Sort
						newvalue = DoBubbleSort(newvalue);
						break;
					case 'I':	// Insertion Sort
						newvalue = DoInsertionSort(newvalue);
						break;
					case 'H':	// Heap Sort
						newvalue = DoHeapSort(newvalue);
						break;
					case 'M':	// Merge Sort
						newvalue = DoMergeSort(newvalue);
						break;
					case 'Q':	// Quick Sort
						newvalue = DoQuickSort(newvalue);
						break;
					case 'R':	// Radix Sort
						newvalue = DoRadixSort(newvalue);
						break;
					case 'X':
						return;	// 프로그램을 종료한다.
					default:
						throw new IOException("잘못된 정렬 방법을 입력했습니다.");
				}
				if (isRandom)
				{
					// 난수일 경우 수행시간을 출력한다.
					System.out.println((System.currentTimeMillis() - t) + " ms");
				}
				else
				{
					// 난수가 아닐 경우 정렬된 결과값을 출력한다.
					for (int i = 0; i < newvalue.length; i++)
					{
						System.out.println(newvalue[i]);
					}
				}

			}
		}
		catch (IOException e)
		{
			System.out.println("입력이 잘못되었습니다. 오류 : " + e.toString());
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoBubbleSort(int[] value){
		for(int index=value.length-1;index>0;index--){
			for(int index2=0;index2<index;index2++){
				if(value[index2]<=value[index2+1])
					continue;
				else{
					int temp=value[index2];
					value[index2] = value[index2+1];
					value[index2+1] = temp;
				}
			}
		}
		return value;
	}
	
	private static int[] DoBubbleSort2(int[] value)
	{
		int startPoint = value.length-1;
		BubbleSort(value, startPoint);
		return (value);
	}

	//Method 설명 : value[num] 자리에 최대값을 넣기 위해, value[0]~value[num]을 탐색하는 과정에서 오름차순으로 swap하는 과정
	//Recursive하게 짜서 num==0이 되면 더 이상 비교할 대상이 없어서 종료
	private static int[] BubbleSort(int[] value, int num){
		if(num==0)
			return value;
		else{
			for(int index=0;index<num;index++){
				if(value[index]<=value[index+1]){
					continue;
				}
				else{
					int temp = value[index];
					value[index] = value[index+1];
					value[index+1] = temp;
				}
			}
			return BubbleSort(value,num-1);
		}
	}
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoInsertionSort(int[] value){
		for(int index=1;index<value.length;index++){
			int insertTarget = value[index];
			for(int index2=index-1;index2>=0;index2--){
				if(value[index2]>insertTarget){
					value[index2+1] = value[index2];
					if(index2==0)
						value[0] = insertTarget;
				}
				else{
					value[index2+1] = insertTarget;
					break;
				}
			}
		}
		return value;
	}
	
	private static int[] DoInsertionSort2(int[] value)
	{
		int startPoint = 1;
		insertionSort(value,startPoint);
		return(value);	
	}

	//Method 설명 : value[1]~value[value.length-1]까지 unsorted part의 값들이 제자리를 찾아가는 과정
	//Recursive하게 작성하여, value[value.length-1에 도달하면 종료
	private static int[] insertionSort(int[] value, int num){
		if(num==value.length)
			return value;
		else{
			int insertTarget = value[num];
			for(int index=num-1;index>=0;index--){
				if(value[index]>insertTarget){
					value[index+1] = value[index];
					if(index==0)
						value[0] = insertTarget;
				}
				else{
					value[index+1] = insertTarget;
					break;
				}
			}
			return insertionSort(value,num+1);
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//Method 설명 : 1)Heap 형태를 만든 뒤, 2)최댓값인 root에 위치한 값을 맨 마지막 위치에 저장하고 recursive하게 array 뒤쪽부터 앞으로 채워나감
	private static int[] DoHeapSort(int[] value)
	{
		buildHeap(value);
		int num =value.length;
		while(num>0){
			int maxValue = value[0];
			value[0] = value[num-1];
			value[num-1] = maxValue;
			num-=1;
			restoreDown(value,0,num-1);
		}
		return (value);
	}
	
	//Method 설명 : Non-leaf node에 대해서 child node에 위치한 값과의 비교를 통해서 root값 산출
	//leaf node의 경우, child node가 없기 때문에 child node와의 비교를 통한 위치조정 작업을 하지 않음
	private static void buildHeap(int[] value){
		int num = value.length;
		//Heap Tree는 Complete Binary Tree방식을 따르기에 leaf-node위치는 value.length/2
		for(int index=(int)(num/2);index>=0;index--)
			restoreDown(value,index,num-1);
	}
	
	
	private static void restoreDown(int[] value,int index, int num){
		int k = value[index];
		int lchild = 2*index+1;
		int rchild = 2*index+2;
		
		while(rchild<=num){
			//CASE1 : k가 가장 큰 값일 때
			if(k>= value[lchild] && k>=value[rchild]){
				value[index] = k;
				return;
			}
			//CASE2 : k가 left child보다 작을 때, right child보다 클 때 
			else if(value[lchild]>value[rchild]){
				value[index] = value[lchild];
				index = lchild;
			}
			
			//CASE3 : k가 right child보다 작고, left child보다 클 때 
			else{
				value[index] = value[rchild];
				index = rchild;
			}		
			lchild = 2*index+1;
			rchild = 2*index+2;
		}
		
		//예외처리 : rchild는 없지만, left child가 존재하며 k가 left child보다 작을 경우
		if(lchild==num && k<value[lchild]){
			value[index] = value[lchild];
			index = lchild;
		}
		value[index] = k;
	}
	

	////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoMergeSort(int[] value)
	{
		value = MergeSort(value,0,value.length-1);
		return (value);
	}
	
	//Method 설명 : value 내 start~end에 위치한 값들에 대해 MergeSort 작업
	//전체 array를 반절로 나누는 작업을 반복해 1개 Node만 남을 경우, 나머지 Node간 Merge 작업
	private static int[] MergeSort(int[] value, int start, int end){
		
		if(start==end){
			int[] newly = new int[1];
			newly[0] = value[start];
			return newly;
		}
		int medium = (start+end)/2;
		int[] list1=MergeSort(value,start,medium);
		int[] list2=MergeSort(value,medium+1,end);
		return merge(list1,list2);
	}

	//Method 설명 : 두 list간에 값 크기 변화를 통해서 Merge 작업
	private static int[] merge(int[] list1, int[] list2){
		int[] merged = new int[list1.length+list2.length];
		int index=0;
		int index1=0;
		int index2=0;
		
		while(index1!=list1.length && index2!=list2.length){
			if(list1[index1]<=list2[index2]){
				merged[index] = list1[index1];
				index++;
				index1++;
			}
			else{
				merged[index] = list2[index2];
				index++;
				index2++;
			}
		}
		
		while(index1!=list1.length){
			merged[index]=list1[index1];
			index++;
			index1++;
		}
		
		while(index2!=list2.length){
			merged[index]=list2[index2];
			index++;
			index2++;
		}
		
		return merged;
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoQuickSort(int[] value)
	{
		QuickSort(value,0,value.length-1);
		return (value);
	}
	
	//Method 설명 : pivot을 기준으로 좌로 pivot보다 작은 값 정렬, 우로 pivot보다 큰 값 정렬
	private static void QuickSort(int[] value, int start, int end){
		if(start>=end)
			return;
		else{
			int pivot = partition(value,start,end);
			QuickSort(value,start,pivot-1);
			QuickSort(value,pivot+1,end);
		}
	}
	
	//Method 설명 : 중앙값을 pivot으로 설정한 뒤, pivot좌측은 pivot보다 작은 값 정렬, 우측은 pivot보다 큰 값 정렬
	private static int partition(int[] value,int start,int end){
		int pivot = (start+end)/2;
		int index1= start;
		int index2 = end;
		
		//STEP1 : pivot 왼쪽 중 pivot보다 큰 값과 pivot 오른쪽 중 pivot보다 작은 값 SWAP
		while(index1<pivot && index2>pivot){
			while(value[index1]<=value[pivot]&&index1<pivot)
				index1++;
			while(value[index2]>=value[pivot]&&index2>pivot)
				index2--;
			if(index1!=pivot && index2!=pivot){
				int temp = value[index1];
				value[index1] = value[index2];
				value[index2] = temp;
				index1++;
				index2--;
			}
		}
		
		//STEP2(CASE1) : pivot 우측의 정렬은 완료되고, 좌측은 정렬이 완료되지 않았을 때 pivot 위치 조정 
		if(index1<pivot){
			int lastS1 = pivot;
			for(int target=pivot-1;target>=index1;target--){
				if(value[target]>value[pivot]){
					--lastS1;
					int temp = value[target];
					value[target] = value[lastS1];
					value[lastS1] = temp;
				}
			}
			int temp = value[pivot];
			value[pivot] = value[lastS1];
			value[lastS1] = temp;
			pivot = lastS1;
		}
		
		//STEP2(CASE2) : pivot 좌측의 정렬은 완료되고, 우측은 정렬이 완료되지 않았을 때 pivot 위치 조정
		else if(index2>pivot){
			int firstS1 = pivot;
			for(int target=pivot+1;target<=index2;target++){
				if(value[target]<value[pivot]){
					++firstS1;
					int temp = value[target];
					value[target] = value[firstS1];
					value[firstS1] = temp;
				}
			}
			int temp = value[pivot];
			value[pivot]= value[firstS1];
			value[firstS1] = temp;
			pivot = firstS1;
		}
		return pivot;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoRadixSort(int[] value){
		int maxDigit = calMaxDigit(value);
		//일의 자리수를 1, 10의 자릿수를 2, 100의 자릿수를 3으로 증가
		int startDigit = 1;
		RadixSort(value,maxDigit,startDigit);
		return (value);
	}
	
	//Method 설명 : 자릿수에 따라서 분류한 뒤, array 형태로 합치는 과정을 재반복
	//** 특수처리 : 부호 +-를 구분하기 위해 최종적으로 부호에 따라 RadixSort 작업 재반복
	private static void RadixSort(int[] value, int maxDigit, int presentDigit){
		//+- 부호에 따라 별도의 sorting 작업
		if(presentDigit>maxDigit){
			signAlign(value);
			return;
		}		
		else{
			pile(value,presentDigit);
			RadixSort(value, maxDigit, presentDigit+1);
		}
	}
	
	//Method설명 : +-에 따라 array 값 재분배 후 array로 다시 합치기
	private static void signAlign(int[] value){
		int[][] storage = new int[2][value.length];
		int[] elementCounter = new int[]{0,0};
		for(int element=0;element<value.length;element++){
			if(value[element]<0){
				storage[0][elementCounter[0]] = value[element];
				elementCounter[0]+=1;
			}
			else{
				storage[1][elementCounter[1]] = value[element];
				elementCounter[1]+=1;
			}
		}
		
		int copyList = 0;
		for(int index1=elementCounter[0]-1;index1>=0;index1--){
			value[copyList] = storage[0][index1];
			copyList++;
		}

		for(int index2=0;index2<elementCounter[1];index2++){
			value[copyList] = storage[1][index2];
			copyList++;
		}
	}
	
	//Method설명 : 자릿수에 따라 array값들을 분류하고 다시 합치는 과정
	private static void pile(int[] value, int presentDigit){
		int[][] storageDigit = new int[10][value.length];
		int[] elementCounter = new int[]{0,0,0,0,0,0,0,0,0,0};
		for(int element=0;element<value.length;element++){
			//STEP1 : value[element]를 자릿수만 뽑아내기
			int targetEle = value[element];
			int preDigitToZero = targetEle % (int)(Math.pow(10, presentDigit));
			int digitNumber = preDigitToZero / (int)(Math.pow(10, presentDigit-1));
			//STEP2 : 자릿수에 따라서 storageDigit에 저장하기
			switch(Math.abs(digitNumber)){
				case 0:
					storageDigit[0][elementCounter[0]] = targetEle;
					elementCounter[0] += 1;
					break;
				case 1:
					storageDigit[1][elementCounter[1]] = targetEle;
					elementCounter[1] += 1;
					break;
				case 2:
					storageDigit[2][elementCounter[2]] = targetEle;
					elementCounter[2] += 1;
					break;
				case 3:
					storageDigit[3][elementCounter[3]] = targetEle;
					elementCounter[3] += 1;
					break;
				case 4:
					storageDigit[4][elementCounter[4]] = targetEle;
					elementCounter[4] += 1;
					break;
				case 5:
					storageDigit[5][elementCounter[5]] = targetEle;
					elementCounter[5] += 1;
					break;
				case 6:
					storageDigit[6][elementCounter[6]] = targetEle;
					elementCounter[6] += 1;
					break;
				case 7:
					storageDigit[7][elementCounter[7]] = targetEle;
					elementCounter[7] += 1;
					break;
				case 8:
					storageDigit[8][elementCounter[8]] = targetEle;
					elementCounter[8] += 1;
					break;
				case 9:
					storageDigit[9][elementCounter[9]] = targetEle;
					elementCounter[9] += 1;
					break;
			}
		}
		//STEP3 : storageDigit의 각 요소를 따 빼내어 다시 value에 저장하기
		int copyList = 0;
		for(int digitPos=0;digitPos<10;digitPos++){
			if(elementCounter[digitPos]==0)
				continue;
			for(int index2=0;index2<elementCounter[digitPos];index2++){
				value[copyList] = storageDigit[digitPos][index2];
				copyList++;
			}
		}
	}
	
	//Method 설명 : 전체 요소들에서 digit의 최댓값 찾기
	private static int calMaxDigit(int[] value){
		int counter=1;
		int maxDigit = 1;
		int[] copy = (int[])value.clone();
		for(int index=0;index<copy.length-1;index++){
			while(copy[index]!=0){
				copy[index] = copy[index]/10;
				counter++;
			}
			if(maxDigit<counter)
				maxDigit = counter;
			counter=1;
		}
		return maxDigit;
	}
	
}